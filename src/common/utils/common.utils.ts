/**
 * Group all utils function that can be not grouped with some other functions
 */

/**
 * A promise to wait some time
 * @param {number} ms time to wait
 * @return {Promise<void>}
 */
export function timeout(ms: number): Promise<void> {
  return new Promise<void>((res) => setTimeout(res, ms));
}

/**
 * Function from mongoose utils. Merge an object containing default values
 * with the same object containing 
 * @param {T} defaults default values for the options 
 * @param {T} options the custom values to merge with the defaults 
 * @return {T} the merged options
 */
export function mergeOptions<T>(defaults: T, options: T): T {
  const keys = Object.keys(defaults);
  let i = keys.length;
  let k: string;
  
  const mergedOptions: T = options || {} as T;

  while (i--) {
    k = keys[i];
    if (!(k in mergedOptions)) {
      mergedOptions[k as keyof T] = defaults[k as keyof T];
    }
  }

  return mergedOptions;
};