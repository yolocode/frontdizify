import { Album } from './album.interface';
import { Artist } from './artist.interface';

export interface Title {
  id: number;
  name: string;
  duration: number;
  artist?: Artist;
  album?: Album;
}