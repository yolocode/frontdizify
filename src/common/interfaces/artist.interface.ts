import { Album } from './album.interface';
import { Title } from './title.interface';

export interface Artist {
  id: number;
  firstname: string;
  lastname: string;
  pictureURI: string;
  titles: Title[];
  // albums?: Album[];
}