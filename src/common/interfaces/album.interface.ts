import { Artist } from './artist.interface';
import { Title } from './title.interface';

export interface Album {
  id: number;
  name: string;
  date: Date;
  pictureURI: string;
  artist?: Artist;
  titles: Title[];
}