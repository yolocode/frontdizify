export interface IBaseMock<T> {
  getFakeData(options?: FakeDataOptions): Promise<T[]>
}

export interface FakeDataOptions {
  withoutFKs?: {[key: string]: boolean}
}