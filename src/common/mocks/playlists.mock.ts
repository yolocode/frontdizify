import { Playlist } from '../interfaces/playlist.interface';
import { ArtistsMock } from './artists.mock';
import { FakeDataOptions, IBaseMock } from './base-mock.interface';
import { TitlesMock } from './titles.mock';

export class PlaylistsMock implements IBaseMock<Playlist>{

  private static instance: PlaylistsMock;

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): PlaylistsMock {
    if (!PlaylistsMock.instance) {
        PlaylistsMock.instance = new PlaylistsMock();
    }

    return PlaylistsMock.instance;
  }

  public getFakeData(options?: FakeDataOptions): Promise<Playlist[]> {
    return new Promise<Playlist[]>(async (res) => {


      const titles = !options?.withoutFKs?.titles
        && await TitlesMock.getInstance().getFakeData();

      const playlists: Playlist[] = [
        {
          id: 1,
          titles: titles
        },
        {
          id: 2,
          titles: titles
        },
        {
          id: 3,
          titles: titles
        },
        {
          id: 4,
          titles: titles
        }
      ]
      res(playlists);
    })
  }
}