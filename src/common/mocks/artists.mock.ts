import { Artist } from '../interfaces/artist.interface';
import { AlbumsMock } from './albums.mock';
import { FakeDataOptions, IBaseMock } from './base-mock.interface';
import { TitlesMock } from './titles.mock';

export class ArtistsMock implements IBaseMock<Artist>{

  private static instance: ArtistsMock;

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): ArtistsMock {
    if (!ArtistsMock.instance) {
      ArtistsMock.instance = new ArtistsMock();
    }

    return ArtistsMock.instance;
  }
  
  public getFakeData(options?: FakeDataOptions): Promise<Artist[]> {
    return new Promise<Artist[]>(async (res) => {

      const titles = !options?.withoutFKs?.titles
        && await TitlesMock.getInstance().getFakeData();

      const artists: Artist[] = [
        { 
          id: 1,
          firstname: "Tigran",
          lastname: "Hamasyan",
          pictureURI: "https://www.jazzradio.fr/media/news/thumb/870x489_5f4c98d8d929b-le-jazz-mutant-de-tigran-hamasyan-m202158.jpg",
          titles: [titles[2]]
        },
        {
          id: 2,
          firstname: "Avishai",
          lastname: "Cohen",
          pictureURI: "https://cdn.radiofrance.fr/s3/cruiser-production/2019/10/3478da9b-685f-484a-b349-ac016e0905d1/1200x680_avishai_cohenterlaak.jpg",
          titles: [titles[5]]
        },
        {
          id: 3,
          firstname: "Rei",
          lastname: "Kondoh",
          pictureURI: "https://yt3.ggpht.com/a/AATXAJwtFICiQcCGtat6_nzKp9c72k9MLWVhuIHml3Xf=s900-c-k-c0x00ffffff-no-rj",
          titles: [titles[8]]
        },
      ]
      res(artists);
    })
  }
}