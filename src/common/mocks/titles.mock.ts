import { Title } from '../interfaces/title.interface';
import { ArtistsMock } from './artists.mock';
import { FakeDataOptions, IBaseMock } from './base-mock.interface';

export class TitlesMock implements IBaseMock<Title> {

  private static instance: TitlesMock;

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): TitlesMock {
    if (!TitlesMock.instance) {
      TitlesMock.instance = new TitlesMock();
    }

    return TitlesMock.instance;
  }

  getFakeData(options?: FakeDataOptions): Promise<Title[]> {
    return new Promise<Title[]>((res) => {
      const titles: Title[] = [
        {
          id: 1,
          name: 'Ara Resurrected',
          duration: 503000,
        },
        {
          id: 2,
          name: 'Space Of Your Existance',
          duration: 283000,
        },
        {
          id: 3,
          name: 'Seattle',
          duration: 131000,
        },
        {
          id: 4,
          name: 'Chutzpan',
          duration: 311000,
        },
        {
          id: 5,
          name: 'Lo Baiom Velo Balyla',
          duration: 326000,
        },
        {
          id: 6,
          name: 'Past Light',
          duration: 356000,
        },
        {
          id: 7,
          name: 'Ara Resurrected',
          duration: 503000,
        },
        {
          id: 8,
          name: 'Thorn In You',
          duration: 384000,
        },
        {
          id: 9,
          name: 'Road Taken',
          duration: 334000,
        },
      ] as Title[]
      res(titles);
    });
  }
}