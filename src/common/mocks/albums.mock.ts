import { Album } from '../interfaces/album.interface';
import { ArtistsMock } from './artists.mock';
import { FakeDataOptions, IBaseMock } from './base-mock.interface';
import { TitlesMock } from './titles.mock';

export class AlbumsMock implements IBaseMock<Album>{

  private static instance: AlbumsMock;

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): AlbumsMock {
    if (!AlbumsMock.instance) {
      AlbumsMock.instance = new AlbumsMock();
    }

    return AlbumsMock.instance;
  }

  public getFakeData(options?: FakeDataOptions): Promise<Album[]> {
    return new Promise<Album[]>(async (res) => {

      const artists = !options?.withoutFKs?.artist
        && await ArtistsMock.getInstance().getFakeData({ withoutFKs: { album: true } });

      const titles = !options?.withoutFKs?.titles
        && await TitlesMock.getInstance().getFakeData();

      const albums: Album[] = [
        {
          id: 1,
          name: "The Call Within",
          pictureURI: "https://images-na.ssl-images-amazon.com/images/I/A1saNCz4uOL._SL1500_.jpg",
          date: new Date(),
          artist: artists && artists[0],
          titles: titles && [titles[0], titles[1]]
        },
        {
          id: 2,
          name: "Gently Disturbed",
          pictureURI: "https://images-na.ssl-images-amazon.com/images/I/61pb665lhVL._SL1200_.jpg",
          date: new Date(),
          artist: artists && artists[1],
          titles: titles && [titles[3], titles[4]]
        },
        {
          id: 3,
          name: "Fire Emblem Fates - OST",
          pictureURI: "https://i1.sndcdn.com/artworks-000201046450-dda1di-t500x500.jpg",
          date: new Date(),
          artist: artists && artists[2],
          titles: titles && [titles[6], titles[7]]
        }
      ]
      res(albums);
    })
  }
}