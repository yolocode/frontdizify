import { User } from '../interfaces/user.interface';
import { IBaseMock } from './base-mock.interface';

export class UsersMock implements IBaseMock<User>{

  private static instance: UsersMock;

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): UsersMock {
    if (!UsersMock.instance) {
      UsersMock.instance = new UsersMock();
    }

    return UsersMock.instance;
  }
  
  public getFakeData(): Promise<User[]> {
    return new Promise<User[]>((res) => {
      const users: User[] = [
        {"id":1, "email":"danyUser3@ynov.com","pseudo":"DD","pictureURI":null,"playlists":[]},
        {"id":2, "email":"pierre.chene@ynov.com","pseudo":"LEOUF","pictureURI":"NoobURI","playlists":[]}
      ]
      res(users);
    })
  }

  public createFakeUser(): Promise<User> {
    return new Promise<User>((res) => {
      const user: User = {"id":1, "email":"test@bartradingx.com","pseudo":"DD","pictureURI":null,"playlists":[]}
      res(user);
    })
  }
}