import { makeAutoObservable } from 'mobx';
import { Playlist } from '../interfaces/playlist.interface';
import { Title } from '../interfaces/title.interface';
import { AuthToken } from '../services/auth-token.service';
import { PlaylistsService } from '../services/playlists.service';
import { FavoritesService } from '../services/favorites.service';
import { UsersService } from '../services/users.service';
import { Favorite } from '../interfaces/favorite.interface';
import { Album } from '../interfaces/album.interface';
import { Artist } from '../interfaces/artist.interface';

interface IUserStore {
  playlists: Playlist[],
  authToken: AuthToken,
  favorite: Favorite
}

export class UserStore implements IUserStore {

  playlistsService: PlaylistsService
  favoritesService: FavoritesService

  authToken: AuthToken
  playlists: Playlist[]
  favorite: Favorite

  constructor() {
    makeAutoObservable(this, { playlistsService: false });
    this.playlistsService = PlaylistsService.getInstance()
    this.favoritesService = FavoritesService.getInstance()
  }

  hydrate(userStoreData: IUserStore) {
    this.playlists = userStoreData?.playlists || [];
    this.authToken = userStoreData?.authToken || null;
    this.favorite = userStoreData?.favorite || null;
  }

  setPlaylists(playlists: Playlist[]) {
    this.playlists = playlists;
  }

  async addPlaylist(playlist: Playlist) {
    await this.playlistsService.create(playlist, this.authToken.token)
    await this.fetchAllPlaylists()
  }

  async addTitleToPlaylist(playlist: Playlist, title: Title) {
    playlist.titles.push(title);
    await this.playlistsService.update(playlist.id, playlist, this.authToken.token)
    await this.fetchAllPlaylists();
  }

  async removeTitleFromPlaylist(playlist: Playlist, title: Title) {
    const i = playlist.titles.findIndex((t: Title) => (t.id === title.id))
    playlist.titles.splice(i, 1)
    await this.playlistsService.update(playlist.id, playlist, this.authToken.token)
    await this.fetchAllPlaylists();
  }

  async fetchAllPlaylists() {
    const playlist = await this.playlistsService.getAll(this.authToken.token);
    this.playlists = [...playlist]
  }

  async fetchFavorite() {
    const favorite = await this.favoritesService.getFavoritesForCurrentUser(this.authToken.token);
    this.favorite = favorite
  }

  async addTitleToFavorites (title: Title) {
    const favorite = await this.favoritesService.createTitleFavorite(title, this.authToken.token)
    this.favorite = favorite
  }

  async addAlbumToFavorites (album: Album) {
    const favorite = await this.favoritesService.createAlbumFavorite(album, this.authToken.token)
    this.favorite = favorite
  }

  async addArtistToFavorites (artist: Artist) {
    const favorite = await this.favoritesService.createArtistFavorite(artist, this.authToken.token)
    this.favorite = favorite
  }

  async removeTitleFromFavorites (title: Title) {
    const favorite = await this.favoritesService.removeTitleFavorite(title, this.authToken.token)
    this.favorite = favorite
  }

  async removeAlbumFromFavorites (album: Album) {
    const favorite = await this.favoritesService.removeAlbumFavorite(album, this.authToken.token)
    this.favorite = favorite
  }

  async removeArtistFromFavorites (artist: Artist) {
    const favorite = await this.favoritesService.removeArtistFavorite(artist, this.authToken.token)
    this.favorite = favorite
  }

}

export async function fetchInitialUserStoreState() {
  // You can do anything to fetch initial store state
  return {} as IUserStore;
}