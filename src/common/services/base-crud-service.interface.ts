export interface IBaseCrudService<T> {
  
  getAll(token?: string): Promise<T[]>;
  create(object?: T, token?: string): Promise<T>;
  getOne(id: number, token?: string): Promise<T>;
  update(id: number, object: T, token?: string): Promise<T>;
  delete(id: number, token?: string): Promise<void>;

} 