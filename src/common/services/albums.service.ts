import { Album } from '../interfaces/album.interface';
import { AlbumsMock } from '../mocks/albums.mock';
import { IBaseCrudService } from './base-crud-service.interface';
import { ApiService } from './api.service';

export class AlbumsService implements IBaseCrudService<Album> {
  
  private static instance: AlbumsService;
  private apiService = ApiService.getInstance();

  private albumsMock = AlbumsMock.getInstance();

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): AlbumsService {
    if (!AlbumsService.instance) {
      AlbumsService.instance = new AlbumsService();
    }

    return AlbumsService.instance;
  }

  count() : Promise<number> {
    return this.apiService.request('get', 'album/count') as Promise<number>;
  }
  getAll(): Promise<Album[]> {
    // TODO: change this using the ApiService class.
    return this.apiService.request('get', 'album') as Promise<Album[]>;
  }

  getByName(name) : Promise<Album[]> {
    return this.apiService.request('get', `album/name/?name=${name}`) as Promise<Album[]>;
  }

  async getTops(): Promise<Album[]> {
    return this.apiService.request('get', 'album/top') as Promise<Album[]>;
  }

  getAllByPage(page: number, size: number): Promise<Album[]> {
    return this.apiService.request('get', `album/?page=${page}&size=${size}`) as Promise<Album[]>;
  }
  
  async getRandoms(): Promise<Album[]> {
    return this.apiService.request('get', 'album/random') as Promise<Album[]>;
  }

  create(album: Album): Promise<Album> {
    throw new Error('Method not implemented.');
  }

  async getOne(id: number): Promise<Album> {
    // TODO: change this using the ApiService class.
    return this.apiService.request('get', `album/${id}`) as Promise<Album>;
  }

  update(id: number, album: Album): Promise<Album> {
    throw new Error('Method not implemented.');
  }

  delete(id: number): Promise<void> {
    throw new Error('Method not implemented.');
  }
  
}