import { Favorite } from '../interfaces/favorite.interface';
import { User } from '../interfaces/user.interface';
import { UsersMock } from '../mocks/users.mock';
import { ApiService } from './api.service';
import { AuthToken } from './auth-token.service';
import { IBaseCrudService } from './base-crud-service.interface';

interface ILoginResult {
  token: string;
  admin: boolean;

}

interface ILoginParam {
  email: string;
  password: string;
}

export class UsersService implements IBaseCrudService<User> {
  
  private static instance: UsersService;

  private usersMock = UsersMock.getInstance();
  private apiService = ApiService.getInstance()

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): UsersService {
    if (!UsersService.instance) {
      UsersService.instance = new UsersService();
    }

    return UsersService.instance;
  }

  getAll(): Promise<User[]> {
    // TODO: change this using the ApiService class.
    return this.usersMock.getFakeData();
  }

  async getCurrentUser(token: string): Promise<User> {
    // TODO: change this using the ApiService class.
    return this.apiService.request('get', 'user/me', { headers: { token: `${token}` }}) as Promise<User>;
  }

  async connect(loginParam: ILoginParam): Promise<User | undefined> {
    const  { token, admin } = await this.apiService.request('post', 'login', { body: { login: loginParam.email, password: loginParam.password } }) as ILoginResult;
    await AuthToken.storeToken(token);

    const user = !admin ? await this.getCurrentUser(token) : undefined;
    return user;
  }

  async checkToken(token: string): Promise<ILoginResult> {
    return this.apiService.request('get', 'check-login', { headers: { token } }) as Promise<ILoginResult>;
  }

  create(user: User): Promise<User> {
    return this.apiService.request('post', 'user', { body: user }) as Promise<User>;
  }

  update(id: number, object: User, token?: string): Promise<User> {
    throw new Error('Method not implemented.');
  }

  updateUserProfile(token: string, user: User): Promise<User> {
    return this.apiService.request('put', 'user/me', {
      headers: { token: `${token}` },
      body: {
        "pseudo": user.pseudo,
        "pictureURI": user.pictureURI
      }
    }) as Promise<User>
  }

  delete(id: number): Promise<void> {
    throw new Error('Method not implemented.');
  }

  async getOne(id: number): Promise<User> {
    throw new Error('Method not implemented.');
  }

  async getFavorites(token: string): Promise<Favorite> {
    return this.apiService.request('get', 'user/favorite', { headers: { token } }) as Promise<Favorite>
  }
  
}