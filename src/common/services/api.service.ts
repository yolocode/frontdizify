import { mergeOptions } from '../utils/common.utils';

/**
 * @interface ApiRequestOptions
 * Define the options for a request.
 * Default options are defined in the ApiService class.
 */
export interface ApiRequestOptions {

  // body of the request
  body?: unknown;

  // headers of the request
  headers?: {[key: string]: string};

  // specify if data return by the API is formatted in Json Api Format.
  isJsonApiData?: boolean;

  // specify if the service request the shily api or another api.
  isRequestingApi?: boolean;
}

/**
 * @type
 * Possibles http request methods
 */
export type HttpMethod = 'get' | 'post' | 'put' | 'delete' | 'patch' | 'head' | 'connect' | 'options' | 'trace'

/**
 * @class
 * Centralize all api requests
 */
export class ApiService {

  private static instance: ApiService;

  private static API_URL = process.env.NEXT_PUBLIC_API_URL;

  // Default options of the request method
  private defaultOptions: ApiRequestOptions = {
    isJsonApiData: true,
    isRequestingApi: true,
  };

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): ApiService {
    if (!ApiService.instance) {
      ApiService.instance = new ApiService();
    }

    return ApiService.instance;
  }

  /**
   * Global api request with options
   * @param {HttpMethod} method http request method
   * @param {string} endpoint api endpoint
   * @param {ApiRequestOptions} options options of the request
   * @return {Promise<unknown>} A promise resolving result of the api request
   */
  public request(
    method: HttpMethod,
    endpoint: string,
    options: ApiRequestOptions = {},
  ): Promise<unknown> {

    // eslint-disable-next-line no-param-reassign
    options = mergeOptions<ApiRequestOptions>(this.defaultOptions, options);

    const url = options.isRequestingApi
      ? `${ApiService.API_URL}/${endpoint}`
      : endpoint;

    return new Promise((resolve, reject) => {
      fetch(url, {
        method,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          ...options.headers,
        },
        ...(!!options.body && { body: JSON.stringify(options.body) }),
      }).then(async (value: Response) => {
        if (!value.ok) reject(await value.json());
        return value.json();
      }).then((value: unknown) => {
        resolve(value);
      }).catch((error: unknown) => {
        reject(error);
      });
    });
  }

}
