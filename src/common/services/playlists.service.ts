import { Playlist } from '../interfaces/playlist.interface';
import { PlaylistsMock } from '../mocks/playlists.mock';
import { IBaseCrudService } from './base-crud-service.interface';
import { ApiService } from './api.service';

export class PlaylistsService implements IBaseCrudService<Playlist> {
  
  private static instance: PlaylistsService;

  private playlistsMock = PlaylistsMock.getInstance();
  private apiService = ApiService.getInstance()

  /**
   * Singleton method to get the only one instance of the class
   */
  public static getInstance(): PlaylistsService {
    if (!PlaylistsService.instance) {
        PlaylistsService.instance = new PlaylistsService();
    }

    return PlaylistsService.instance;
  }

  getAll(token: string): Promise<Playlist[]> {
    return this.apiService.request('get', 'user/playlist', { headers: { token: `${token}` }}) as Promise<Playlist[]>;
  }

  create(object: Playlist, token: string): Promise<Playlist> {
    return this.apiService.request('post', 'user/playlist', { headers: { token: `${token}` }, body: object}) as Promise<Playlist>;
  }

  async getOne(id: number, token: string): Promise<Playlist> {
    return this.apiService.request('get', `user/playlist/${id}`, { headers: { token: `${token}` }}) as Promise<Playlist>;
  }

  update(id: number, playlist: Playlist, token: string): Promise<Playlist> {
    return this.apiService.request('post', 'user/playlist', { headers: { token: `${token}` }, body: playlist}) as Promise<Playlist>
  }

  delete(id: number): Promise<void> {
    throw new Error('Method not implemented.');
  }
  
}