import { NextPage, NextPageContext } from "next";
import React, { Component } from "react";
import { UserLevel } from '../common/enums/user-level.enum';
import { AuthToken } from '../common/services/auth-token.service';
import { RedirectService } from '../common/services/redirect.service';
import { UsersService } from '../common/services/users.service';

export type AuthProps = {
  authToken: AuthToken
}

/**
 * Auth Guard to protect route from non authenticated users. Redirect to login route.
 * @param {NextPage} WrappedComponent protected page 
 * @param {UserLevel} level level protection 
 * @return {NextPage} the protected page
 */
export function privateRoute(WrappedComponent, level?: UserLevel) {

  return class extends Component<AuthProps> {

    static async getInitialProps(ctx: NextPageContext) {

      let initialProps = {};

      try {
        // retrieve auth token from request cookies
        const authToken = await AuthToken.fromNext(ctx);
        initialProps = { authToken };

        // check expiration
        if (authToken.isExpired) throw Error();

        // check the level
        if (level === UserLevel.Admin && authToken.level !== UserLevel.Admin) throw Error()
        if (level === UserLevel.User && authToken.level !== UserLevel.User) throw Error()
      } catch {
        RedirectService.redirectToLogin(ctx.res);
      } finally {

        // Call wrapped component get inital props
        if (WrappedComponent.getInitialProps) {
          const wrappedProps = await WrappedComponent.getInitialProps(ctx);
          return { ...wrappedProps, ...initialProps };
        }
        return initialProps;
      }
    }

    render() {
      const { authToken, ...propsWithoutAuth } = this.props;
      return <WrappedComponent authToken={authToken} {...propsWithoutAuth} />;
    }
  };
}