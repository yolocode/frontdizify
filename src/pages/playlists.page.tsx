import React, { useEffect, useState } from 'react';
import { BaseLayout } from '../../src/components/layout.component'
import { Playlist } from '../../src/common/interfaces/playlist.interface';
import { NextPage, NextPageContext } from 'next';
import { PlaylistsService } from '../../src/common/services/playlists.service';
import PlaylistList from '../components/playlist-list.component';
import { Layout, Button } from 'antd';
import { AuthToken } from '../common/services/auth-token.service';
import { privateRoute } from '../hoc/private-route.hoc';
import { inject, observer } from 'mobx-react';
import { UserStore } from '../common/stores/user.store';
import { UserLevel } from '../common/enums/user-level.enum';


interface IProps {
  userStore?: UserStore;
}

const PlaylistsPage: NextPage<IProps> = inject((stores: Record<string, unknown>) => ({
  userStore: stores.userStore,
}))(observer((props: IProps) => {

  async function handleAddPlaylist() {
    props.userStore.addPlaylist({titles: []} as Playlist);
  }

  return (
    <div>
      <h1>Mes playlists</h1>
      <Button onClick={handleAddPlaylist}>Ajouter une playlist</Button>
  
      <PlaylistList playlists={props.userStore.playlists} />
    </div>
  )
}))

export default privateRoute(PlaylistsPage, UserLevel.User);


