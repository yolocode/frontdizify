import React from 'react';
import {NextPage, NextPageContext} from 'next';
import {Button, Form, Image, Input, notification} from 'antd';
import {useRouter} from 'next/router';
import {User} from '../common/interfaces/user.interface'
import {UsersService} from "../common/services/users.service";
import {AuthToken} from "../common/services/auth-token.service";

interface IProps {
  user: User,
  authToken: AuthToken,
}

const UserPage: NextPage<IProps> = (props: IProps) => {
  const layout = {
    labelCol: {span: 8},
    wrapperCol: {span: 16},
  };

  const onFinish = async (values) => {
    const user: User = {
      pseudo: values.pseudo ?? props.user.pseudo,
      pictureURI: values.picture ?? props.user.pictureURI,
      id: null,
      email: null,
      playlists: null,
      password: null,
      token: null
    };

    UsersService.getInstance().updateUserProfile(props.authToken.token, user).then((user: User) => {
      
      notification.success({
        message: "L'utilisateur a été modifié"
      })
      props.user.pseudo = user.pseudo
      props.user.pictureURI = user.pictureURI
    }).catch((reason) => {
      notification.error({
        message: "Erreur lors de la modification de l'utilisateur"
      })
    })
  };

  return (
    <div className="user-page">

      <div className="user-page-header">
        <h1>Modification du profil</h1>
        <div className="user-infos">
          <h3 className="user-email">{`${props.user.email}`}</h3>
          <Image alt={props.user.pseudo} src={props.user.pictureURI} width={200} />
        </div>
      </div>
      <Form
        {...layout}
        name="basic"
        initialValues={{["pseudo"]: props.user.pseudo, ["picture"]: props.user.pictureURI}}
        onFinish={onFinish}
      >
        <Form.Item
          label="Pseudo"
          name="pseudo"
          rules={[{required: true, message: 'Votre pseudo'}]}>
          <Input/>
        </Form.Item>
        <Form.Item
          label="Image de profil"
          name="picture"
          rules={[{required: true, message: 'Adresse de votre image de profil'}]}>
          <Input/>
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Appliquer les changements
          </Button>
        </Form.Item>
      </Form>
    </div>
  )
}

UserPage.getInitialProps = async function (context: NextPageContext) {
  const authToken = await AuthToken.fromNext(context);
  const user: User = await UsersService.getInstance().getCurrentUser(authToken.token);
  return { user } as IProps;
}

export default UserPage;