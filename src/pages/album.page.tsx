import React from 'react';
import { Album } from '../../src/common/interfaces/album.interface';
import { NextPage, NextPageContext } from 'next';
import { AlbumsService } from '../../src/common/services/albums.service';
import { Row, Image, Col, Space, Avatar } from 'antd';
import { HeartFilled, HeartOutlined }from '@ant-design/icons';
import TitleList from '../components/title-list.component';
import { inject, observer } from 'mobx-react';
import { UserStore } from '../common/stores/user.store';
import { UserLevel } from '../common/enums/user-level.enum';

interface IProps {
  album: Album
  userStore?: UserStore,
}

const AlbumPage: NextPage<IProps> = inject((stores: Record<string, unknown>) => ({
  userStore: stores.userStore
}))(observer(
  (props: IProps) => {
  function isInFavorites () {
    let album = props.userStore.favorite?.albums.find(e => e.id === props.album.id);
    return !!album
  }
  return (
    <div className="album-page">

      {/* Album page header */}
      <div className="album-page-header">
        <Image
          className="album-page-image"
          src={props.album.pictureURI}
        />
        <div className="album-infos">
          <h1 className="album-title">{props.album.name}</h1>
          <div className="album-sub-title">
            {/* <Image height="100%" className="artist-image" src={props.album.artist?.pictureURI} /> */}
            <Avatar size="small" src={props.album.artist?.pictureURI } />
            <p className="artist-name">{`${props.album.artist?.firstname} ${props.album.artist?.lastname}`}</p>
            <div className="text-dot-separator"></div>
            <p className="album-date">{(new Date(props.album.date)).toLocaleDateString('fr-FR', { day: 'numeric', month: 'long', year: 'numeric'})}</p>
            {props.userStore.authToken?.level === UserLevel.User && <div>
              {isInFavorites()
                ? <HeartFilled style={ {color: "#FF69B4", marginLeft: "10px" } }  title="Retirer des favoris" onClick={() => props.userStore.removeAlbumFromFavorites(props.album)}/>
                : <HeartOutlined style={ {color: "#FF69B4", marginLeft: "10px"} } title="Ajouter aux favoris" onClick={() => props.userStore.addAlbumToFavorites(props.album)}/>
              }
              </div>
            }
          </div>
        </div>
      </div>

      {/* Titles */}
      <h2>Titres</h2>
      <TitleList titles={props.album.titles} />
    </div>
  )
}))

AlbumPage.getInitialProps = async function(context: NextPageContext) {
  const { albumId } = context.query;
  const albumsService = AlbumsService.getInstance();
  const album = await albumsService.getOne(Number(albumId));
  return { album } as IProps;
}

export default AlbumPage;


