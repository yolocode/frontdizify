import React, { useState, useEffect } from 'react';
import { Album } from '../../src/common/interfaces/album.interface';
import { NextPage, NextPageContext } from 'next';
import { AlbumsService } from '../../src/common/services/albums.service';
import AlbumList from '../components/album-list.component';
import { Pagination} from 'antd';
import Search from 'antd/lib/input/Search';
import Space from '../components/space.component';

interface IProps {
  albums: Album[],
  albumCount: number
}

const sizePage = 12;

const AlbumsPage: NextPage<IProps> = (props: IProps) => {
  
  const albumsService = AlbumsService.getInstance();

  const [albums, setAlbums] = useState<Album[]>(props.albums);
  const [search, setSearch] = useState<string>('');
  const [pageNumber, setPageNumber] = useState<number>(1);

  useEffect(() => {
    setAlbums(props.albums)
  },  [props.albums]);

  async function onChange(pageNumber: number) {
    setAlbums(await albumsService.getAllByPage(pageNumber - 1, sizePage));
    setPageNumber(pageNumber);
  }
  
  async function onSearch(value: string) {
    const albums = value
      ? await albumsService.getByName(value)
      : await albumsService.getAllByPage(pageNumber - 1, sizePage)

    setAlbums(albums);      
    setSearch(value);
  }

  return (
    <div>
      <div className="page-title-block">
        <h1>Albums</h1>
        <Search
          placeholder="Rechercher des albums"
          allowClear
          onSearch={onSearch} 
          style={{ width: 400, margin: '0 10px' }}
        />
      </div>

      <Space height={64} />

      <AlbumList albums={albums} />

      <Space height={64} />

      <div className="pagination-line">
        <Pagination
          disabled={!!search}
          defaultCurrent={1}
          pageSize={sizePage}
          total={props.albumCount}
          onChange={onChange}
          current={pageNumber}
        />
      </div>
    </div>
  )
}

AlbumsPage.getInitialProps = async function(context: NextPageContext) {
  const albumsService = AlbumsService.getInstance();
  const albums = await albumsService.getAllByPage(0,sizePage);
  const albumCount = await albumsService.count();
  return { albums, albumCount } as IProps;
}

export default AlbumsPage;
