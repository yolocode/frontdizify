import React from 'react';
import { NextPage, NextPageContext } from 'next';
import { Album } from '../common/interfaces/album.interface';
import { Artist } from '../common/interfaces/artist.interface';
import { AlbumsService } from '../common/services/albums.service';
import { ArtistsService } from '../common/services/artists.service';
import ArtistList from '../components/artist-list.component';
import AlbumList from '../components/album-list.component';
import Space from '../components/space.component';

interface IProps {
  topArtists: Artist[];
  topAlbums: Album[];
}

const HomePage: NextPage<IProps> = (props: IProps) => {

  return (
    <div className="home-page">
      <div className="home-page-header">
        <h1>Dizify</h1>
      </div>
      <Space height={64} />

      <h2 className="home-page-sub-title">Meilleurs artistes</h2>
      <ArtistList artists={props.topArtists} />
      <Space height={64} />

      <h2 className="home-page-sub-title">Meilleurs albums</h2>
      <AlbumList albums={props.topAlbums} />
    </div>
  )
}

HomePage.getInitialProps = async function(context: NextPageContext) {
  const artistsService = ArtistsService.getInstance();
  const albumsService = AlbumsService.getInstance();
  const topArtists = await artistsService.getTops();
  const topAlbums = await albumsService.getTops();

  return { topArtists: topArtists, topAlbums: topAlbums } as IProps;
}

export default HomePage;


