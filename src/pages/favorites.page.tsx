import React from 'react';
import { NextPage, NextPageContext } from 'next';
import ArtistList from '../components/artist-list.component';
import AlbumList from '../components/album-list.component';
import Space from '../components/space.component';
import TitleList from '../components/title-list.component';
import { UsersService } from '../common/services/users.service';
import { Favorite } from '../common/interfaces/favorite.interface';
import { AuthToken } from '../common/services/auth-token.service';
import { UserLevel } from '../common/enums/user-level.enum';
import { privateRoute } from '../hoc/private-route.hoc';

interface IProps {
  favorite: Favorite
}

const FavoritesPage: NextPage<IProps> = (props: IProps) => {

  return (
    <div className="favorite-page">
      <h1>Favoris</h1>
      <Space height={64} />

      <h2>Vos artistes favoris</h2>
      <ArtistList artists={props.favorite.artists} />
      <Space height={64} />

      <h2>Vos albums favoris</h2>
      <AlbumList albums={props.favorite.albums} />
      <Space height={64} />

      <h2>Vos titres favoris</h2>
      <TitleList titles={props.favorite.titles} />
    </div>
  )
}

FavoritesPage.getInitialProps = async function(context: NextPageContext) {
  const authToken = await AuthToken.fromNext(context);

  const usersService = UsersService.getInstance();
  const favorite = await usersService.getFavorites(authToken.token);

  return { favorite } as IProps;
}

export default privateRoute(FavoritesPage, UserLevel.User);


