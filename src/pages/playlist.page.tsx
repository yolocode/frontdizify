import React from 'react';
import { Playlist } from '../../src/common/interfaces/playlist.interface';
import { NextPage, NextPageContext } from 'next';
import { PlaylistsService } from '../../src/common/services/playlists.service';
import { Row, Image, Col, Space, Avatar } from 'antd';
import TitleList from '../components/title-list.component';
import { AuthToken } from '../common/services/auth-token.service';
import { inject, observer } from 'mobx-react';
import { UserStore } from '../common/stores/user.store';
import { UserLevel } from '../common/enums/user-level.enum';
import { privateRoute } from '../hoc/private-route.hoc';

interface IProps {
  playlist: Playlist
  userStore?: UserStore
}

const PlaylistPage: NextPage<IProps> = inject((stores: Record<string, unknown>) => ({
  userStore: stores.userStore
}))(observer((props: IProps) => {

  /**
   * Trick to make reactive sub components.
   */
  function getPlaylistFromStore(): Playlist {
    return props.userStore.playlists.find((playlist: Playlist) => (
      playlist.id === props.playlist.id
    ))
  }

  return (
    <div className="album-page">
      {/* Titles */}
      <h2>Titres</h2>
      <TitleList titles={getPlaylistFromStore()?.titles} />
    </div>
  )
}))

PlaylistPage.getInitialProps = async function(context: NextPageContext) {
  const { playlistId } = context.query;
  const playlistsService = PlaylistsService.getInstance();
  const authToken = await AuthToken.fromNext(context);
  const playlist = await playlistsService.getOne(Number(playlistId), authToken.token);
  return { playlist } as IProps;
}

export default privateRoute(PlaylistPage, UserLevel.User);


