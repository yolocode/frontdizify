import React from 'react';
import { Artist } from '../../src/common/interfaces/artist.interface';
import { NextPage, NextPageContext } from 'next';
import { ArtistsService } from '../../src/common/services/artists.service';
import { Avatar } from 'antd';
import AlbumList from '../components/album-list.component';
import { Album } from '../common/interfaces/album.interface';
import TitleList from '../components/title-list.component';
import { inject, observer } from 'mobx-react';
import { HeartFilled, HeartOutlined }from '@ant-design/icons';
import { UserStore } from '../common/stores/user.store';
import { UserLevel } from '../common/enums/user-level.enum';

interface IProps {
  artist: Artist,
  albums: Album[]
  userStore?: UserStore,
}

const ArtistPage: NextPage<IProps> = inject((stores: Record<string, unknown>) => ({
  userStore: stores.userStore
}))(observer(
  (props: IProps) => {

  function isInFavorites () {
    let artist = props.userStore.favorite?.artists.find(e => e.id === props.artist.id);
    return !!artist
  }
  return (
    <div className="artist-page">
      <div className="artist-page-header">
        <Avatar size={254} alt={props.artist.firstname} src={props.artist.pictureURI} />
        <div className="artist-infos">
          <h1 className="artist-title">{`${props.artist.firstname} ${props.artist.lastname}`}</h1>
          <div className="artist-sub-title-container">
            <p className="artist-sub-title">{`${props.albums.length} album(s)`}</p>
            {props.userStore.authToken?.level === UserLevel.User && <div>
              {isInFavorites()
                ? <HeartFilled style={ {color: "#FF69B4", marginLeft: "10px" } }  title="Retirer des favoris" onClick={() => props.userStore.removeArtistFromFavorites(props.artist)}/>
                : <HeartOutlined style={ {color: "#FF69B4", marginLeft: "10px"} } title="Ajouter aux favoris" onClick={() => props.userStore.addArtistToFavorites(props.artist)}/>
              }
              </div>
            }
          </div>
        </div>
      </div>

      <h2>Albums</h2>
      <AlbumList albums={props.albums} cardSizes={{lg: 4}} />

      <h2>Singles</h2>
      <TitleList titles={props.artist.titles} />
    </div>
  )
}))

ArtistPage.getInitialProps = async function(context: NextPageContext) {
  const { artistId } = context.query;
  const artistsService = ArtistsService.getInstance();
  const artist = await artistsService.getOne(Number(artistId));
  const albums = await artistsService.getAlbums(Number(artistId));
  return { artist, albums } as IProps;
}

export default ArtistPage;


