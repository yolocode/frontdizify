import { Col, Row } from 'antd';
import React from 'react';
import { Artist } from '../common/interfaces/artist.interface';
import ArtistCard from './artist-card.component';

interface IProps {
  artists: Artist[];
}

const ArtistList: React.FC<IProps> = (props: IProps) => {
  
  return (
    <Row>
      {props.artists.map((artist: Artist) => (
        <Col lg={6} md={8} sm={12} style={{ padding: 8 }} key={artist.id}>
          <ArtistCard key={artist.id} artist={artist} />
        </Col>
      ))}
    </Row>
  )
}

export default ArtistList;