import { List } from 'antd';
import React from 'react';
import { Playlist } from '../common/interfaces/playlist.interface';
import PaylistListItem from './playlist-list-item.component';

interface IProps {
  playlists: Playlist[]
}

const PlaylistList: React.FC<IProps> = (props: IProps) => {
  return (
    <List
      className="playlist-list-component"
      itemLayout="horizontal"
      dataSource={props.playlists}
      renderItem={(playlist => (<PaylistListItem playlist={playlist}/>))}
    />
  )
}

export default PlaylistList;