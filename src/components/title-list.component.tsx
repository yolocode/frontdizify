import { List } from 'antd';
import React from 'react';
import { Title } from '../common/interfaces/title.interface';
import TitleListItem from './title-list-item.component';

interface IProps {
  titles: Title[]
}

const TitleList: React.FC<IProps> = (props: IProps) => {
  return (
    <List
      className="title-list-component"
      itemLayout="horizontal"
      dataSource={props.titles}
      renderItem={(title => (<TitleListItem title={title} />))}
    />
  )
}

export default TitleList;