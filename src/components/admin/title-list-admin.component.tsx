import * as React from "react";
import { List, Datagrid, TextField, NumberField } from 'react-admin';

interface IProps {

}

export const TitleListAdmin: React.FC<IProps> = (props: IProps) => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <NumberField source="id" />
            <TextField source="name" />
            <NumberField source="duration" />
        </Datagrid>
    </List>
);