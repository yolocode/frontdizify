import * as React from "react";
import { List, Datagrid, TextField, EmailField, NumberField, DateField } from 'react-admin';

interface IProps {

}

export const ArtistListAdmin: React.FC<IProps> = (props: IProps) => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <NumberField source="id" />
            <TextField source="firstname" />
            <TextField source="lastname" />
            <TextField source="pictureURI" />
        </Datagrid>
    </List>
);