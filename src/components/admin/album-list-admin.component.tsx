import * as React from "react";
import { List, Datagrid, TextField, NumberField, DateField, ReferenceField } from 'react-admin';

interface IProps {

}

export const AlbumListAdmin: React.FC<IProps> = (props: IProps) => (
    <List {...props}>
        <Datagrid rowClick="edit">
            <NumberField source="id" />
            <TextField source="name" />
            <DateField source="date" />
            <TextField source="pictureURI" />
            <ReferenceField label="Artist" source="artist.id" reference="artist">
                <TextField source="id" />
            </ReferenceField>
            {/* <ReferenceManyField label="Titles" source="titles" reference="title" target="albumId">
                <SingleFieldList>
                    <ChipField source="title.name" />
                </SingleFieldList>
            </ReferenceManyField> */}
        </Datagrid>
    </List>
);