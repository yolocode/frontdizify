import { Dropdown, List, Menu } from 'antd';
import React from 'react';
import { Playlist } from '../common/interfaces/playlist.interface';
import { MoreOutlined } from '@ant-design/icons';
import Link from 'next/link';
import { Title } from '../common/interfaces/title.interface';

interface IProps {
  playlist: Playlist
}

const TitleListItem: React.FC<IProps> = (props: IProps) => {

  function getPlaylistLength(playlist: Playlist): string {
    return `${playlist.titles.length} titre(s)`;
  }

  function getPLaylistTime(playlist: Playlist): string {
    let seconds = 0;
    for (let title of playlist.titles) {
      seconds += title.duration;
    }
    const remainingSeconds = seconds % 60;
    const minutes = (seconds - remainingSeconds) / 60;
    if (remainingSeconds < 10) {
      return `${minutes}:0${remainingSeconds}`;
    } else {
      return `${minutes}:${remainingSeconds}`;
    }
  }

  return (
    <Link href={`/playlists/${props.playlist.id}`}>
      <List.Item className="playlist-list-item">
        <List.Item.Meta
          title={getPlaylistLength(props.playlist)}
        />
        <div className="playlist-item-end-content">
          <p>{getPLaylistTime(props.playlist)}</p>
        </div>
      </List.Item>
    </Link>
  )
}

export default TitleListItem;