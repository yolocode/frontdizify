import React from 'react';
import { Album } from '../common/interfaces/album.interface';
import { Card, Image } from 'antd';
import Link from 'next/link';

const { Meta } = Card;

interface IProps {
  album: Album;
}

const AlbumCard: React.FC<IProps> = (props: IProps) => {
  
  return (
    <Link href={`/albums/${props.album.id}`}>
      <Card
        hoverable
        style={{ width: "100%", borderRadius: 8, overflow: 'hidden' }}
        cover={<Image alt={props.album.name} src={props.album.pictureURI} />}
        className="album-card"
      >
        <Meta
          title={props.album.name}
          description={(new Date(props.album.date).toLocaleDateString('fr-FR'))}
        />
      </Card>
    </Link>
  )
}

export default AlbumCard;