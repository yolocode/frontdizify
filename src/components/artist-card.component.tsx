import React from 'react';
import { Artist } from '../common/interfaces/artist.interface';
import { Card } from 'antd';
import Link from 'next/link';
import Avatar from 'antd/lib/avatar/avatar';

const { Meta } = Card;

interface IProps {
  artist: Artist;
}

const ArtistCard: React.FC<IProps> = (props: IProps) => {
  
  return (
    <Link href={`/artists/${props.artist.id}`}>
      <Card
        hoverable
        style={{ width: "100%", borderRadius: 8 }}
        // cover={<img alt={props.artist.firstname} src={props.artist.pictureURI} />}
        className="artist-card"
      >
        <Meta
          avatar={<Avatar src={props.artist.pictureURI} size={64} />}
          title={`${props.artist.firstname} ${props.artist.lastname}`}
          // description={`${props.artist.albums?.length} album(s)`}
        />
      </Card>
    </Link>
  )
}

export default ArtistCard;