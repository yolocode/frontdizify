import { Col, Row } from 'antd';
import React, { useEffect } from 'react';
import { Album } from '../common/interfaces/album.interface';
import AlbumCard from './album-card.component';

interface IProps {
  albums: Album[];
  cardSizes?: {
    lg?: number;
    md?: number;
    sm?: number;
  }
}

const AlbumList: React.FC<IProps> = (props: IProps) => {
  
  return (
    <Row>
      {props.albums.map((album: Album) => (
        <Col
          lg={props.cardSizes?.lg || 6}
          md={props.cardSizes?.md || 8}
          sm={props.cardSizes?.lg || 24}
          style={{ padding: 8 }}
          key={album.id}
        >
          <AlbumCard key={album.id} album={album} />
        </Col>
      ))}
    </Row>
  )
}

export default AlbumList;