import { NextPageContext } from 'next'
import React, { useEffect, useState } from 'react'
import { AuthToken } from '../src/common/services/auth-token.service'
import { BaseLayout } from '../src/components/layout.component'
import '../styles/globals.scss'

import { inject, observer, Provider } from 'mobx-react';
import { fetchInitialUserStoreState, UserStore } from '../src/common/stores/user.store'
import { PlaylistsService } from '../src/common/services/playlists.service'
import App, { AppContext, AppInitialProps } from 'next/app'
import { Router, withRouter } from 'next/router'
import { Layout } from 'antd'
import { UsersService } from '../src/common/services/users.service'

interface IProps extends AppInitialProps {
  authToken: AuthToken,
  initialUserStoreState: any,
  router: Router

}

interface IState {
  userStore: UserStore
}

class MyApp extends App<IProps> {

  state: IState = {
    userStore: new UserStore()
  };

  /**
   * Get the auth token if exist, and get the default user store value
   * @param {AppContext} appContext the app context
   * @return {IProps} the necessary props to give to all the pages
   */
  static async getInitialProps(appContext: AppContext): Promise<IProps> {
    const appProps = await App.getInitialProps(appContext)
    const initialUserStoreState = await fetchInitialUserStoreState()
    const authToken = await AuthToken.fromNext(appContext.ctx);
    
    return { ...appProps, initialUserStoreState: { ...initialUserStoreState, authToken }, authToken } as IProps
  };

  // Hydrate serialized state to store
  static getDerivedStateFromProps(props: IProps, state: IState) {
    state.userStore.hydrate(props.initialUserStoreState);
    return state;
  }

  render() {
    const { Component, pageProps, authToken } = this.props;

    return (
      <Provider userStore={this.state.userStore}>

        { this.props.router.pathname.split('/')[1] !== 'admin' && 
          <BaseLayout {...pageProps} authToken={authToken} >
            <Component {...pageProps} authToken={authToken} />
          </BaseLayout>
        }
        
        { this.props.router.pathname.split('/')[1] === 'admin' && <Component {...pageProps} authToken={authToken} /> }
          
      </Provider>
    )
  }
}

export default withRouter(MyApp);
